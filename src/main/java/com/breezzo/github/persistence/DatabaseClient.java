package com.breezzo.github.persistence;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by breezzo on 17.07.16.
 */
public class DatabaseClient {

    private static final DbSettings dbSettings = DbSettings.load();

    private Cluster cluster;

    public DatabaseClient() {
        this.cluster = Cluster.builder()
                .addContactPoint(dbSettings.host)
                .build();
    }

    public Session openSession() {
        return cluster.connect(dbSettings.keySpace);
    }

    public void close() {
        cluster.close();
    }

    private static class DbSettings {
        String host;
        String keySpace;

        static DbSettings load() {
            Properties props = new Properties();
            try (InputStream stream = DatabaseClient.class.getResourceAsStream("/persistence.properties")) {
                props.load(stream);
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }

            DbSettings result = new DbSettings();
            result.host = props.getProperty("host");
            result.keySpace = props.getProperty("keySpace");

            return result;
        }
    }
}
