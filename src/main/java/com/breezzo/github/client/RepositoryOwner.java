package com.breezzo.github.client;

/**
 * Created by breezzo on 15.07.16.
 */
public class RepositoryOwner {
    private Long id;
    private String login;
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
