package com.breezzo.github.client;

/**
 * Created by breezzo on 15.07.16.
 */
public class RepositorySearchInfo {
    private String q;
    private Order order;
    private Sort sort;

    public String getQuery() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public Order getOrder() {
        return order;
    }

    public RepositorySearchInfo setOrder(Order order) {
        this.order = order;
        return this;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public enum Sort {
        STARS("stars"),
        FORKS("forks"),
        UPDATED("updated");

        Sort(String querySyntax) {
            this.querySyntax = querySyntax;
        }

        private final String querySyntax;

        public String querySyntax() {
            return querySyntax;
        }
    }
}
