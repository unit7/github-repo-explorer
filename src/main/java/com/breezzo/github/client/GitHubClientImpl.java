package com.breezzo.github.client;

import com.breezzo.github.GitHubApi;
import retrofit.RestAdapter;

import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class GitHubClientImpl implements GitHubClient {

    @Override
    public RepositorySearchResult findRepositories(RepositorySearchInfo searchInfo) {
        GitHubApi gitHubApi = buildClient();
        return gitHubApi.findRepositories(
                searchInfo.getQuery(),
                searchInfo.getSort().querySyntax(),
                searchInfo.getOrder().querySyntax()
        );
    }

    @Override
    public List<RepositoryInfo> listRepositories(UserRepositorySearchInfo searchInfo) {
        GitHubApi gitHubApi = buildClient();
        return gitHubApi.listRepositories(
                searchInfo.getUsername(),
                searchInfo.getType(),
                searchInfo.getSort(),
                searchInfo.getDirection()
        );
    }

    private GitHubApi buildClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(GitHubApi.ENDPOINT)
                .build();

        return restAdapter.create(GitHubApi.class);
    }
}
