package com.breezzo.github.persistence;

import com.datastax.driver.core.Session;
import com.google.common.collect.ImmutableMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by breezzo on 17.07.16.
 */
public class DatabaseClientTest {

    private static DatabaseClient client;

    @BeforeClass
    public static void setupClass() {
        client = new DatabaseClient();
        try (Session session = client.openSession()) {
            session.execute("create table test(id int primary key, name text)");
        }
    }

    @AfterClass
    public static void tearDownClass() {
        try (Session session = client.openSession()) {
            session.execute("drop table test");
        }
        client.close();
    }

    @Test
    public void modifying() {
        try (Session session = client.openSession()) {
            session.execute("insert into test(id, name) values (1, '123')");
            session.execute("insert into test(id, name) values (2, '123')");
        }
        try (Session session = client.openSession()) {
            session.execute("insert into test(id, name) values (3, '4234234')");
            session.execute("insert into test(id, name) values (:id, :name)", ImmutableMap.of(
                    "id", 5,
                    "name", "kole"
            ));
        }
        try (Session session = client.openSession()) {
            session.execute("delete from test where id = 1");
        }
    }
}
