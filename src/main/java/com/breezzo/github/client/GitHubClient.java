package com.breezzo.github.client;

import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public interface GitHubClient {

    RepositorySearchResult findRepositories(RepositorySearchInfo searchInfo);

    List<RepositoryInfo> listRepositories(UserRepositorySearchInfo searchInfo);

    //List<> downloadRepositories();
}
