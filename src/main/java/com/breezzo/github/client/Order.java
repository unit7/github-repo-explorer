package com.breezzo.github.client;

/**
 * Created by breezzo on 24.07.16.
 */
public enum Order {
    ASC("asc"),
    DESC("desc");

    Order(String querySyntax) {
        this.querySyntax = querySyntax;
    }

    private final String querySyntax;

    public String querySyntax() {
        return querySyntax;
    }
}
