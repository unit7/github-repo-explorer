package com.breezzo.github.client;

/**
 * Created by breezzo on 15.07.16.
 */
public class UserRepositorySearchInfo {
    private String username;
    private String type;
    private String sort;
    private String direction;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
