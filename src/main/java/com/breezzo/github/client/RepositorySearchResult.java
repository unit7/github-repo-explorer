package com.breezzo.github.client;

import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class RepositorySearchResult {
    private Integer total_count;
    private List<RepositoryInfo> items;

    public Integer getTotal_count() {
        return total_count;
    }

    public RepositorySearchResult setTotal_count(Integer total_count) {
        this.total_count = total_count;
        return this;
    }

    public List<RepositoryInfo> getItems() {
        return items;
    }

    public RepositorySearchResult setItems(List<RepositoryInfo> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RepositorySearchResult{");
        sb.append("total_count=").append(total_count);
        sb.append(", items=").append(items);
        sb.append('}');
        return sb.toString();
    }
}
