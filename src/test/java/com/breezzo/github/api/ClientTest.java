package com.breezzo.github.api;

import com.breezzo.github.client.*;
import com.breezzo.github.persistence.DatabaseClient;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class ClientTest {
    private static final Logger logger = LoggerFactory.getLogger(ClientTest.class);

    private static DatabaseClient client;

    @BeforeClass
    public static void setupClass() {
        client = new DatabaseClient();
        try (Session session = client.openSession()) {
            session.execute("create table if not exists repository(id bigint primary key, name text, ownerlogin text)");
            session.execute("create table if not exists repository_search(" +
                    "repo_id bigint primary key, query text, name text, ownerlogin text)");
        }
    }

    @AfterClass
    public static void tearDownClass() {
        client.close();
    }

    @Test
    public void saveRepoTest() {
        GitHubClientImpl gitHubClient = new GitHubClientImpl();
        UserRepositorySearchInfo searchInfo = new UserRepositorySearchInfo();
        searchInfo.setUsername("unit7-0");
        List<RepositoryInfo> reposInfo = gitHubClient.listRepositories(searchInfo);

        try (Session session = client.openSession()) {
            for (RepositoryInfo repo : reposInfo) {
                RepositoryOwner owner = repo.getOwner();
                session.execute(QueryBuilder.insertInto("repository")
                        .values(Arrays.asList("id", "name", "ownerlogin"),
                                Arrays.asList(repo.getId(), repo.getName(), owner.getLogin())));
            }
        }
    }

    @Test
    public void findRepoTest() {
        GitHubClientImpl gitHubClient = new GitHubClientImpl();
        RepositorySearchInfo searchInfo = new RepositorySearchInfo();
        searchInfo.setQ("cassandra java");
        searchInfo.setOrder(Order.ASC);
        searchInfo.setSort(RepositorySearchInfo.Sort.UPDATED);

        RepositorySearchResult repositories = gitHubClient.findRepositories(searchInfo);
        logger.info("query: {}, count: {}", searchInfo.getQuery(), repositories.getTotal_count());

        try (final Session session = client.openSession()) {
            for (RepositoryInfo info : repositories.getItems()) {
                session.execute(QueryBuilder.insertInto("repository_search")
                        .values(Arrays.asList("query", "repo_id", "name", "ownerlogin"),
                                Arrays.asList(
                                        searchInfo.getQuery(),
                                        info.getId(),
                                        info.getName(),
                                        info.getOwner().getLogin()))
                );
            }
        }
    }
}
