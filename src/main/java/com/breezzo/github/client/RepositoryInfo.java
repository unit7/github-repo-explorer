package com.breezzo.github.client;

/**
 * Created by breezzo on 15.07.16.
 */
public class RepositoryInfo {
    private Long id;
    private RepositoryOwner owner;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RepositoryOwner getOwner() {
        return owner;
    }

    public void setOwner(RepositoryOwner owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RepositoryInfo{");
        sb.append("id=").append(id);
        sb.append(", owner=").append(owner);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
