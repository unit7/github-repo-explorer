package com.breezzo.github;

import com.breezzo.github.client.RepositoryInfo;
import com.breezzo.github.client.RepositorySearchResult;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public interface GitHubApi {

    String ENDPOINT = "https://api.github.com";

    @GET("/users/{username}/repos")
    List<RepositoryInfo> listRepositories(@Path("username") String username,
                                              @Query("type") String type,
                                              @Query("sort") String sort,
                                              @Query("direction") String direction);

    @GET("/search/repositories")
    RepositorySearchResult findRepositories(@Query("q") String query,
                                            @Query("sort") String sort,
                                            @Query("order") String order);
}
